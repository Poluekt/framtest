/**************************************************************************//*****
 * @file     stdio.c
 * @brief    Implementation of newlib syscall
 ********************************************************************************/

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/times.h>
#include "stm32f1xx_hal.h"

#undef errno
//extern int errno;
//extern int _end;

caddr_t _sbrk(int incr)
{

	   extern char _ebss;
	    static char *heap_end;
	    char *prev_heap_end;

	    if (heap_end == 0)
	    {
	        heap_end = &_ebss;
	    }
	    prev_heap_end = heap_end;

	    char * stack = (char*) __get_MSP();
	    if (heap_end + incr > stack)
	    {
	 //       _write(STDERR_FILENO, "Heap and stack collision\n", 25);
	 //       errno = ENOMEM;
	        return (caddr_t) -1;
	        //abort ();
	    }

	    heap_end += incr;
	    return (caddr_t) prev_heap_end;

//	static unsigned char *heap = NULL;
//	unsigned char *prev_heap;
//
//	if (heap == NULL)
//	{
//		heap = (unsigned char *) &_end;
//	}
//	prev_heap = heap;
//
//	heap += incr;
//
//	return (caddr_t) prev_heap;
}

clock_t _times_r(struct tms *buf)
{
	return -1;
}
int link(char *old, char *new)
{
	return -1;
}

int _close(int file)
{
	return -1;
}

int _fstat(int file, struct stat *st)
{
	st->st_mode = S_IFCHR;
	return 0;
}

int _isatty(int file)
{
	return 1;
}

int _lseek(int file, int ptr, int dir)
{
	return 0;
}

int _read(int file, char *ptr, int len)
{
	return 0;
}

int _write(int file, char *ptr, int len)
{
	return len;
}

void __errno(void)
{
	/* Abort called */
//	while (1)
		;
}

void abort(void)
{
	/* Abort called */
	while (1)
		;
}


//http://www.scs.stanford.edu/histar/src/pkg/uclibc/libc/sysdeps/linux/arm/


/* Copy memory like memcpy, but no return value required.  */
void __aeabi_memcpy(void *dest, const void *src, size_t n)
{
	(void) memcpy(dest, src, n);
}

/* Copy memory like memcpy, but no return value required.  */
void __aeabi_memcpy4(void *dest, const void *src, size_t n)
{
	(void) memcpy(dest, src, n);
}

void __aeabi_memset(void *dest, size_t n, int c)
{
	(void) memset(dest, c, n);
}
void __aeabi_memmove(void * dest, const void * src, size_t n)
{
	(void) memmove(dest, src, n);
}

void __aeabi_memclr4(void *dest, size_t n)
{
	(void) memset(dest, 0, n);
}

void __aeabi_memclr(void *dest, size_t n)
{
	(void) memset(dest, 0, n);
}

/* Versions of the above which may assume memory alignment.  */
//strong_alias (__aeabi_memclr, __aeabi_memclr4)
//strong_alias (__aeabi_memclr, __aeabi_memclr8)
/* --------------------------------- End Of File ------------------------------ */
