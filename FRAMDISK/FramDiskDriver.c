/*
 * FramDiskDriver.c
 *
 *  Created on: 1 мая 2018 г.
 *      Author: Poluekt
 */

#include "stm32f1xx_hal.h"
#include "FM24V10_Driver.h"
#include "FramDiskDriver.h"

I2C_HandleTypeDef hi2c1;
DMA_HandleTypeDef hdma_i2c1_rx;
DMA_HandleTypeDef hdma_i2c1_tx;

extern uint16_t A1, A2;

void FramDisk_GPIO_Init(void)
{

 GPIO_InitTypeDef GPIO_InitStruct;

 /* GPIO Ports Clock Enable */
 __HAL_RCC_GPIOD_CLK_ENABLE();
 __HAL_RCC_GPIOA_CLK_ENABLE();
 __HAL_RCC_GPIOB_CLK_ENABLE();

 /*Configure GPIO pin Output Level */
 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);
 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);

 /*Configure GPIO pin : AO A1 */
 GPIO_InitStruct.Pin = GPIO_PIN_4 | GPIO_PIN_5;
 GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
 HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

 hi2c1.Instance = I2C1;
 hi2c1.Init.ClockSpeed = 1000000;
 hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
 hi2c1.Init.OwnAddress1 = 0;
 hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
 hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
 hi2c1.Init.OwnAddress2 = 0;
 hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
 hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
 if (HAL_I2C_Init(&hi2c1) != HAL_OK)
 {
   _Error_Handler(__FILE__, __LINE__);
 }

}
bool FramDisk_BufferWrite(uint8_t* pBuffer, uint32_t length, uint32_t WriteAddress){
	uint32_t len;
	if(!length)		return false;
//--------------
	for (int var = 0; var < 4; var++){
		if(WriteAddress < ((var + 1) * DEVICE_MEMORY_VALUE)){
			len = ((WriteAddress + length) > ((var + 1) * DEVICE_MEMORY_VALUE)) ?
					(((var + 1) * DEVICE_MEMORY_VALUE) - WriteAddress) : length;
			switch(var){
				case 0:
					A1 = 0;
					A2 = 1;
					break;
				case 1:
					A1 = 1;
					A2 = 0;
					break;
				case 2:
					A1 = 1;
					A2 = 1;
					break;
				case 3:
					A1 = 0;
					A2 = 0;
					break;
			}
			if(!FM24V10_BufferWrite(pBuffer, len, WriteAddress - (var * DEVICE_MEMORY_VALUE))){
				return false;
			}
			length -= len;
			pBuffer += len;
			WriteAddress += len;
		}
		if(!length)
			return true;
	}
//--------------
//	if(WriteAddress < DEVICE_MEMORY_VALUE){
//		len = ((WriteAddress + length) > DEVICE_MEMORY_VALUE) ? (DEVICE_MEMORY_VALUE - WriteAddress) : length;
//		if(!FM24V10_BufferWrite(pBuffer, len, WriteAddress)){
//			return false;
//		}
//		length -=len;
//		pBuffer +=len;
//		WriteAddress +=len;
//	}
//	if(!length)return true;
////--------------
//
//	if(WriteAddress < (2*DEVICE_MEMORY_VALUE)){
//		len = ((WriteAddress + length) > (2*DEVICE_MEMORY_VALUE)) ? ((2*DEVICE_MEMORY_VALUE) - WriteAddress) : length;
//		if(!FM24V10_BufferWrite(pBuffer, len, WriteAddress-DEVICE_MEMORY_VALUE)){
//			return false;
//		}
//		length -=len;
//		pBuffer +=len;
//		WriteAddress +=len;
//	}
//	if(!length)return true;
////--------------
//	if(WriteAddress < (3*DEVICE_MEMORY_VALUE)){
//		len = ((WriteAddress + length) > (3*DEVICE_MEMORY_VALUE)) ? ((3*DEVICE_MEMORY_VALUE) - WriteAddress) : length;
//		if(!FM24V10_BufferWrite(pBuffer, len, WriteAddress-(2*DEVICE_MEMORY_VALUE))){
//			return false;
//		}
//		length -=len;
//		pBuffer +=len;
//		WriteAddress +=len;
//	}
//	if(!length)return true;
//	if(WriteAddress < (4*DEVICE_MEMORY_VALUE)){
//		len = ((WriteAddress + length) > (4*DEVICE_MEMORY_VALUE)) ? ((4*DEVICE_MEMORY_VALUE) - WriteAddress) : length;
//		if(!FM24V10_BufferWrite(pBuffer, len, WriteAddress-(3*DEVICE_MEMORY_VALUE))){
//			return false;
//		}
//		length -=len;
//		pBuffer +=len;
//		WriteAddress +=len;
//	}
//	if(!length)return true;
	return false;
}

bool FramDisk_BufferRead(uint8_t* pBuffer, uint32_t length, uint32_t ReadAddress){
	uint32_t len;
	if(!length)		return false;
//--------------
	for (int var = 0; var < 4; var++){
		if(ReadAddress < ((var + 1) * DEVICE_MEMORY_VALUE)){
			len = ((ReadAddress + length) > ((var + 1) * DEVICE_MEMORY_VALUE)) ?
					(((var + 1) * DEVICE_MEMORY_VALUE) - ReadAddress) : length;
			len = (len> 0x10000)?0x10000:len;
			switch(var){
				case 0:
					A1 = 0;
					A2 = 1;
					break;
				case 1:
					A1 = 1;
					A2 = 0;
					break;
				case 2:
					A1 = 1;
					A2 = 1;
					break;
				case 3:
					A1 = 0;
					A2 = 0;
					break;
			}
			if(!FM24V10_BufferRead(pBuffer, len, ReadAddress - (var * DEVICE_MEMORY_VALUE))){
				return false;
			}
			length -= len;
			pBuffer += len;
			ReadAddress += len;
		}
		if(!length)
			return true;
	}

	return false;
}
void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(hi2c->Instance==I2C1)
  {
  /* USER CODE BEGIN I2C1_MspInit 0 */

  /* USER CODE END I2C1_MspInit 0 */

    /**I2C1 GPIO Configuration
    PB6     ------> I2C1_SCL
    PB7     ------> I2C1_SDA
    */
    GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* Peripheral clock enable */
    __HAL_RCC_I2C1_CLK_ENABLE();
    __HAL_RCC_DMA1_CLK_ENABLE();
    /* I2C1 DMA Init */
    /* I2C1_RX Init */
    hdma_i2c1_rx.Instance = DMA1_Channel7;
    hdma_i2c1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_i2c1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_i2c1_rx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_i2c1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_i2c1_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_i2c1_rx.Init.Mode = DMA_NORMAL;
    hdma_i2c1_rx.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_i2c1_rx) != HAL_OK)
    {
      _Error_Handler(__FILE__, __LINE__);
    }

    __HAL_LINKDMA(hi2c,hdmarx,hdma_i2c1_rx);

    /* I2C1_TX Init */
    hdma_i2c1_tx.Instance = DMA1_Channel6;
    hdma_i2c1_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hdma_i2c1_tx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_i2c1_tx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_i2c1_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_i2c1_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_i2c1_tx.Init.Mode = DMA_NORMAL;
    hdma_i2c1_tx.Init.Priority = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_i2c1_tx) != HAL_OK)
    {
      _Error_Handler(__FILE__, __LINE__);
    }

    __HAL_LINKDMA(hi2c,hdmatx,hdma_i2c1_tx);

    HAL_NVIC_SetPriority(I2C1_EV_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);
    HAL_NVIC_SetPriority(I2C1_ER_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(I2C1_ER_IRQn);

    HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);

    HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);

  }

}

void HAL_I2C_MspDeInit(I2C_HandleTypeDef* hi2c)
{

  if(hi2c->Instance==I2C1)
  {
  /* USER CODE BEGIN I2C1_MspDeInit 0 */

  /* USER CODE END I2C1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_I2C1_CLK_DISABLE();

    /**I2C1 GPIO Configuration
    PB6     ------> I2C1_SCL
    PB7     ------> I2C1_SDA
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_6|GPIO_PIN_7);

    /* I2C1 DMA DeInit */
    HAL_DMA_DeInit(hi2c->hdmarx);
    HAL_DMA_DeInit(hi2c->hdmatx);

    /* I2C1 interrupt DeInit */
    HAL_NVIC_DisableIRQ(I2C1_EV_IRQn);
    HAL_NVIC_DisableIRQ(I2C1_ER_IRQn);
  /* USER CODE BEGIN I2C1_MspDeInit 1 */

  /* USER CODE END I2C1_MspDeInit 1 */
  }

}
