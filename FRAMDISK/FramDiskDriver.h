/*
 * FramDiskDriver.h
 *
 *  Created on: 1 мая 2018 г.
 *      Author: Poluekt
 */

#ifndef FRAMDISK_FRAMDISKDRIVER_H_
#define FRAMDISK_FRAMDISKDRIVER_H_

void FramDisk_GPIO_Init(void);
bool FramDisk_BufferWrite(uint8_t* pBuffer, uint32_t length, uint32_t WriteAddress);
bool FramDisk_BufferRead(uint8_t* pBuffer, uint32_t length, uint32_t ReadAddress);

#endif /* FRAMDISK_FRAMDISKDRIVER_H_ */
