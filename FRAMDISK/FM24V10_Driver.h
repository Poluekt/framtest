/*
 * I2C_Driver.h
 *
 *  Created on: 23 ���� 2013 �.
 *      Author: develop2
 */

#ifndef __I2C_Driver_H
#define __I2C_Driver_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "stdbool.h"
#include "string.h"
#include "FramDiskDriver.h"

#define I2C_Regim
//#define I2C_USE_RX_DMA
#define I2C_USE_TX_DMA
#define I2C_USE_IT 1


#define SCL_H         GPIOB->BSRR = GPIO_PIN_6
#define SCL_L         GPIOB->BRR  = GPIO_PIN_6

#define SDA_H         GPIOB->BSRR = GPIO_PIN_7
#define SDA_L         GPIOB->BRR  = GPIO_PIN_7

#define SCL_read      GPIOB->IDR  & GPIO_PIN_6
#define SDA_read      GPIOB->IDR  & GPIO_PIN_7

#define  I2C_DEVICEADDRESS     0xA0
#define  DEVICE_MEMORY_VALUE    128*1024

//=========================== PUBLIC ==================================================================================
bool FM24V10_BufferWrite(uint8_t* pBuffer, uint32_t length, uint32_t WriteAddress);
void TEST(void);
bool FM24V10_BufferRead(uint8_t* pBuffer, uint32_t length, uint32_t WriteAddress);
void I2C_Stop(void);
//============================================================================================================================



#endif
