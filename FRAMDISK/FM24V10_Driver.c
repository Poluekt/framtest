#include "stm32f1xx_hal.h"
#include "FM24V10_Driver.h"
#include "cmsis_os.h"

uint16_t  A1, A2;
extern I2C_HandleTypeDef hi2c1;
extern DMA_HandleTypeDef hdma_i2c1_rx;
extern DMA_HandleTypeDef hdma_i2c1_tx;
uint8_t const DeviceAddress = 0xA0;

static bool WritePage(uint32_t WriteAddress, uint32_t len, uint8_t* pBuffer);
static bool ReadPage(uint32_t WriteAddress, uint32_t len, uint8_t* pBuffer);
//static void FillMassiv(uint8_t * massiv,uint16_t len);
//=============================================================================================
static void FillMassiv(uint8_t * massiv,uint16_t len){
	for (int var = 0; var < len; var++){
		massiv[var] = (uint8_t) var + 0x44;
		if(var == 10)
			massiv[var] = 20;
		if(var == 21)
			massiv[var] = 40;
		if(var == 26)
			massiv[var] = 2;
		if(var == 44)
			massiv[var] = 99;
		if(var == 56)
			massiv[var] = 66;
		if(var == 73)
			massiv[var] = 77;
		if(var == 95)
			massiv[var] = 62;
		if(var == 124)
			massiv[var] = 32;
	}
}
//====================================================================================================================
bool FM24V10_BufferWrite(uint8_t* pBuffer, uint32_t length, uint32_t WriteAddress){
	uint32_t len;
	uint32_t AdrNextPage;
	uint8_t pBuffer2[128];

	length = ((WriteAddress + length) > 128 * 1024) ? (128 * 1024 - WriteAddress) : length;

	while(length){
		AdrNextPage = (WriteAddress + 128) & 0xFFFFFF80;
		len = ((WriteAddress + length) > AdrNextPage) ? (AdrNextPage - WriteAddress) : length;
		len = (len > 128) ? 128 : len;

		if(!(WritePage(WriteAddress, len, pBuffer))){
			return false;
		}
		memset(pBuffer2,0,128);
		if(!(ReadPage(WriteAddress, len, pBuffer2))){
			return false;
		}
		if(memcmp(pBuffer, pBuffer2, len) != 0){
			return false;
		}
		pBuffer += len;
		WriteAddress += len;
		length -= len;
	}
	return true;
}
//====================================================================================================================
bool FM24V10_BufferRead(uint8_t* pBuffer, uint32_t length, uint32_t WriteAddress){

return	ReadPage( WriteAddress,  length,  pBuffer);

}
//====================================================================================================================
void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *I2cHandle){
	Error_Handler();
}
void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *I2cHandle){
}
void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *I2cHandle){
}
void I2C1_EV_IRQHandler(void){
	HAL_I2C_EV_IRQHandler(&hi2c1);
}
void I2C1_ER_IRQHandler(void){
	HAL_I2C_ER_IRQHandler(&hi2c1);
}
void DMA1_Channel6_IRQHandler(void){
	HAL_DMA_IRQHandler(&hdma_i2c1_tx);
}
void DMA1_Channel7_IRQHandler(void){
	HAL_DMA_IRQHandler(&hdma_i2c1_rx);
}
//==============================================================================================================
static bool ReadPage(uint32_t WriteAddress, uint32_t len, uint8_t* pBuffer){
	uint32_t err;
	uint8_t DEVADR;
	HAL_StatusTypeDef volatile ret;
	DEVADR = DeviceAddress + ((WriteAddress >> 15) & 2) + (A1 << 3) + (A2 << 2); // A2=A1=0; RW=0;
	uint16_t MemAddress = WriteAddress & 0x0000FFFF;

	ret= HAL_I2C_Mem_Write(&hi2c1, (uint16_t) DEVADR, MemAddress,	I2C_MEMADD_SIZE_16BIT, pBuffer,
			                (uint16_t) 0, 40);
	if(ret != HAL_OK){
		return false;
	}
	DEVADR +=1;

#ifdef I2C_USE_RX_DMA

	ret= HAL_I2C_Master_Receive_DMA(&hi2c1, (uint16_t) DEVADR, pBuffer,(uint16_t) len);
	err=HAL_I2C_GetError(&hi2c1);
	  if (/*(err != HAL_I2C_ERROR_AF)||*/(err != HAL_I2C_ERROR_NONE))
	    {
	      Error_Handler();
	    }
	  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
	  {
	  }
	  if(ret == HAL_OK)return true;else return false;
#elif   I2C_USE_IT

	ret= HAL_I2C_Master_Receive_IT(&hi2c1, (uint16_t) DEVADR, pBuffer,(uint16_t) len);
	err=HAL_I2C_GetError(&hi2c1);
//	  if (/*(err != HAL_I2C_ERROR_AF)||*/(err != HAL_I2C_ERROR_NONE))
//	    {
//	      Error_Handler();
//	    }
	  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
	  {
	  }
	  if(ret == HAL_OK)return true;else return false;
#else
	ret= HAL_I2C_Master_Receive(&hi2c1,(uint16_t)DEVADR,pBuffer,(uint16_t)len,20);
	if(ret != HAL_OK){
		return false;
	}
	return true;
#endif





}

//==============================================================================================================
void TEST(void){
	uint8_t M1[155], M2[155];
	uint16_t volatile A, B, C,C1;
	bool volatile ret, ret1, F;
	bool volatile retb,retb1;
	volatile uint8_t m1, m2;

	FillMassiv(M1,sizeof(M1));
	memset(M2, 0, 155);
   uint32_t adr=0/*128*1024*/;

	 retb= FramDisk_BufferRead(M2, 155, adr);
	 retb= FramDisk_BufferRead(M2, 155, adr);
	 retb= FramDisk_BufferRead(M2, 155, adr);
	 retb= FramDisk_BufferRead(M2, 155, adr);
HAL_Delay(100);
	 uint32_t ll=128*1024*4;
	while(ll >= 155){
		A = TIM4->CNT;
		 retb= FramDisk_BufferWrite(M1, 155, adr);
		B = TIM4->CNT;
		C = B - A;
		memset(M2, 0, 155);
		A = TIM4->CNT;
		retb1= FramDisk_BufferRead(M2, 155, adr);
		B = TIM4->CNT;
		C1 = B - A;
		for (int var = 0; var < 155; var++){
			m1 = M1[var];
			m2 = M2[var];
			if(M1[var] != M2[var])
				F = 1;
		}
		ll -=155;
		adr +=155;
	}
}

//==============================================================================================================
static bool WritePage(uint32_t WriteAddress, uint32_t len, uint8_t* pBuffer){
	uint32_t err;
	uint8_t DEVADR;
	HAL_StatusTypeDef volatile ret;
	DEVADR = DeviceAddress + ((WriteAddress >> 15) & 2) + (A1 << 3) + (A2 << 2); // A2=A1=0; RW=0;
	uint16_t MemAddress=WriteAddress&0x0000FFFF;


#ifdef  I2C_USE_TX_DMA
	ret= HAL_I2C_Mem_Write_DMA(&hi2c1, (uint16_t) DEVADR, MemAddress,	I2C_MEMADD_SIZE_16BIT, pBuffer,
			                (uint16_t) len);
	err=HAL_I2C_GetError(&hi2c1);
//	  if (/*(err != HAL_I2C_ERROR_AF)||*/(err != HAL_I2C_ERROR_NONE))
//	    {
//	      Error_Handler();
//	    }
	  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
	  {
	  }
	  if(ret == HAL_OK)return true;else return false;


#elif I2C_USE_IT >0


	ret= HAL_I2C_Mem_Write_IT(&hi2c1, (uint16_t) DEVADR, MemAddress,	I2C_MEMADD_SIZE_16BIT, pBuffer,
			                (uint16_t) len);
	err=HAL_I2C_GetError(&hi2c1);
//	  if (/*(err != HAL_I2C_ERROR_AF)||*/(err != HAL_I2C_ERROR_NONE))
//	    {
//	      Error_Handler();
//	    }
	  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
	  {
	  }
	  if(ret == HAL_OK)return true;else return false;

#else
	ret= HAL_I2C_Mem_Write(&hi2c1, (uint16_t) DEVADR, MemAddress,	I2C_MEMADD_SIZE_16BIT, pBuffer,
			                (uint16_t) len, 20);
	if(ret != HAL_OK){
		return false;
	}
	return true;





#endif
}
//====================================================================================================================
