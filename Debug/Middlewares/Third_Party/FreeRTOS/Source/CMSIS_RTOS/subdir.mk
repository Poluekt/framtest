################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.c 

OBJS += \
./Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.o 

C_DEPS += \
./Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/%.o: ../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"G:\PROJECT_FRAM\FRAM_DISK\Drivers\CMSIS\Device\ST\STM32F1xx\Include" -I"G:\PROJECT_FRAM\FRAM_DISK\FRAMDISK" -I"G:\PROJECT_FRAM\FRAM_DISK\Middlewares\Third_Party\FreeRTOS\Source\portable\GCC\ARM_CM3" -I"G:\PROJECT_FRAM\FRAM_DISK\Middlewares\Third_Party\FreeRTOS\Source\CMSIS_RTOS" -I"G:\PROJECT_FRAM\FRAM_DISK\Middlewares\Third_Party\FreeRTOS\Source\include" -I"G:\PROJECT_FRAM\FRAM_DISK\Middlewares\Third_Party\FatFs\src" -I"G:\PROJECT_FRAM\FRAM_DISK\Inc" -I"G:\PROJECT_FRAM\FRAM_DISK\Drivers\STM32F1xx_HAL_Driver\Inc\Legacy" -I"G:\PROJECT_FRAM\FRAM_DISK\Drivers\STM32F1xx_HAL_Driver\Inc" -I"G:\PROJECT_FRAM\FRAM_DISK\Drivers\CMSIS\Include" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


