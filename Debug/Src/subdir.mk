################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/fatfs.c \
../Src/freertos.c \
../Src/main.c \
../Src/stm32f1xx_hal_msp.c \
../Src/stm32f1xx_it.c \
../Src/system_stm32f1xx.c \
../Src/user_diskio.c 

OBJS += \
./Src/fatfs.o \
./Src/freertos.o \
./Src/main.o \
./Src/stm32f1xx_hal_msp.o \
./Src/stm32f1xx_it.o \
./Src/system_stm32f1xx.o \
./Src/user_diskio.o 

C_DEPS += \
./Src/fatfs.d \
./Src/freertos.d \
./Src/main.d \
./Src/stm32f1xx_hal_msp.d \
./Src/stm32f1xx_it.d \
./Src/system_stm32f1xx.d \
./Src/user_diskio.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"G:\PROJECT_FRAM\FRAM_DISK\Drivers\CMSIS\Device\ST\STM32F1xx\Include" -I"G:\PROJECT_FRAM\FRAM_DISK\FRAMDISK" -I"G:\PROJECT_FRAM\FRAM_DISK\Middlewares\Third_Party\FreeRTOS\Source\portable\GCC\ARM_CM3" -I"G:\PROJECT_FRAM\FRAM_DISK\Middlewares\Third_Party\FreeRTOS\Source\CMSIS_RTOS" -I"G:\PROJECT_FRAM\FRAM_DISK\Middlewares\Third_Party\FreeRTOS\Source\include" -I"G:\PROJECT_FRAM\FRAM_DISK\Middlewares\Third_Party\FatFs\src" -I"G:\PROJECT_FRAM\FRAM_DISK\Inc" -I"G:\PROJECT_FRAM\FRAM_DISK\Drivers\STM32F1xx_HAL_Driver\Inc\Legacy" -I"G:\PROJECT_FRAM\FRAM_DISK\Drivers\STM32F1xx_HAL_Driver\Inc" -I"G:\PROJECT_FRAM\FRAM_DISK\Drivers\CMSIS\Include" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


